import { controls } from '../../constants/controls';

const pressedKeys = new Set();
const fightState = [
  {damage: 0, actions: [], superHitAvailability: true, size: 'left', prefix: 'One'},
  {damage: 0, actions: [], superHitAvailability: true, size: 'right', prefix: 'Two'},
];

const fightMap = {attacker: 0, defender: 1}

function toggleKeyPress(e) {
  const { code } = e;
  // in case holding button is forbidden;
  // if (e.type === 'keydown' && pressedKeys.has(code)) {
  //   return;
  // }
  if (e.type === 'keydown') {
    pressedKeys.add(code);
  } else if (e.type === 'keyup') {
    pressedKeys.delete(code);
  }
}


function clearStateActions() {
  fightState.forEach((playerMeta) => (playerMeta.actions = []));
}

function clearState() {
  fightState.forEach((playerMeta) => (playerMeta = {}));
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    function attackAction() {
      if (pressedKeys.size === 0) {
        return;
      }

      const actions = retrieveActions();
      saveFighterActions(0, actions);
      saveFighterActions(1, actions);

      // compute potential damage
      fightMap.attacker = 0;
      fightMap.defender = 1;
      const secondFighterDamage = getDamage(firstFighter, secondFighter);
      
      fightMap.attacker = 1;
      fightMap.defender = 0;
      const firstFighterDamage = getDamage(secondFighter, firstFighter);

      updateFighterDamage(firstFighter, 0, firstFighterDamage);
      updateFighterDamage(secondFighter, 1, secondFighterDamage);

      hanldeSuperHit(0);
      hanldeSuperHit(1);

      const winner = checkWinner(firstFighter, 0, secondFighter, 1);
      if (winner) {
        clearState();
        stopListening();
        resolve(winner);
      }
      clearStateActions();
    }

    function handleKeyAction(e) {
      toggleKeyPress(e);
      attackAction(e);
    }

    document.addEventListener('keydown', handleKeyAction);
    document.addEventListener('keyup', handleKeyAction);

    function stopListening() {
      document.removeEventListener('keydown', handleKeyAction);
      document.removeEventListener('keyup', handleKeyAction);
    }
  });
}

export function getDamage(attacker, defender) {
  const attackPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const attackerFighterMeta = fightState[fightMap.attacker];
  const defenderFighterMeta = fightState[fightMap.defender];
  let damage = 0;
  if (attackerFighterMeta.actions.includes('attack')) {
    damage += attackPower;
  }
  if (defenderFighterMeta.actions.includes('block')) {
    damage -= blockPower;
  }
  if (attackerFighterMeta.actions.includes('superhit') && attackerFighterMeta.superHitAvailability) {
    damage = attacker.attack * 2;
  }

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + (2 - 1) * Math.random();
  const attackPower = fighter.attack * criticalHitChance;
  return attackPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + (2 - 1) * Math.random();
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

// FIGHT HELPERS

// ACTIONS
function retrieveActions() {
  const actions = Object.keys(controls).filter((key) => {
    const valueArr = typeof controls[key] === 'string' ? [controls[key]] : controls[key];
    return valueArr.every((item) => pressedKeys.has(item));
  });
  return actions;
}

function saveFighterActions(index, actions) {
  const transformMap = {
    PlayerOneAttack: 'attack',
    PlayerOneBlock: 'block',
    PlayerTwoAttack: 'attack',
    PlayerTwoBlock: 'block',
    PlayerOneCriticalHitCombination: 'superhit',
    PlayerTwoCriticalHitCombination: 'superhit',
  };
  const fighterMeta = fightState[index];
  const rule = new RegExp(fighterMeta.prefix);
  let fighterActions = actions.filter((action) => action.match(rule)).map((action) => transformMap[action]);

  if (fighterActions.indexOf('block') !== -1) {
    fighterActions = ['block'];
  }
  fighterMeta.actions = fighterActions;
}

// DAMAGE
function updateFighterDamage(fighter, index, damage) {
  if (!damage) {
    return;
  }

  const fighterMeta = fightState[index];
  fighterMeta.damage += damage;
  const healthLeft = fighterMeta.damage >= fighter.health ? 0 : (1 - fighterMeta.damage / fighter.health) * 100;
  updateHealthLine(fighterMeta.size, healthLeft);
}

function updateHealthLine(size, healthLeft) {
  const lineId = `${size}-fighter-indicator`;
  const healthLineElement = document.getElementById(lineId);
  healthLineElement.style.width = `${healthLeft}%`;
}

// SUPER HIT
function hanldeSuperHit(index) {
  const fighterMeta = fightState[index];
  if (!fighterMeta.actions.includes('superhit')) {
    return;
  }
  const superHitTimeout = 10000;
  fighterMeta.superHitAvailability = false;
  clearSuperHitIndicator(fighterMeta.size, superHitTimeout);
  setTimeout(() => {
    fighterMeta.superHitAvailability = true;
  }, superHitTimeout);
}

function clearSuperHitIndicator(size, timeout) {
  const id = `${size}-fighter-indicator-superhit`;
  const indicator = document.getElementById(id);
  indicator.classList.add('arena___fighter-superhit_restore');
  setTimeout(() => {
    indicator.classList.remove('arena___fighter-superhit_restore');
  }, timeout);
}

// CHECK WINNER
function checkWinner(firstFighter, firstFighterIndex, secondFighter, secondFighterIndex) {
  let winner = null;
  const { damage: firstFighterDamage } = fightState[firstFighterIndex];
  const { damage: secondFighterDamage } = fightState[secondFighterIndex];
  if (firstFighterDamage >= firstFighter.health) {
    winner = secondFighter;
  } else if (secondFighterDamage >= secondFighter.health) {
    winner = firstFighter;
  }
  return winner;
}
