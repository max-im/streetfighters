import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterContent = constructfighterContent(fighter);
    fighterElement.append(fighterContent);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function constructfighterContent(fighter) {
  const maxLevel = 5;
  const maxHealth = 60;
  const fighterImage = createFighterImage(fighter);
  const fighterContent = createElement({ tagName: 'div', className: 'fighter-preview___content' });
  const fighterName = createElement({ tagName: 'h3', className: 'fighter-preview___name' });
  const fighterAtack = createElement({
    tagName: 'p',
    className: 'fighter-preview___level fighter-preview___level_attack',
    attributes: { title: 'attack', style: `width: ${(fighter.attack / maxLevel) * 100}%` },
  });
  const fighterDefense = createElement({
    tagName: 'p',
    className: 'fighter-preview___level fighter-preview___level_defense',
    attributes: { title: 'defense', style: `width: ${(fighter.defense / maxLevel) * 100}%` },
  });
  const fighterHealth = createElement({
    tagName: 'p',
    className: 'fighter-preview___level fighter-preview___level_health',
    attributes: { title: 'health', style: `width: ${(fighter.health / maxHealth) * 100}%` },
  });
  const fighterAttackText = createElement({ tagName: 'p', className: 'fighter-preview___label' });
  const fighterDefenseText = createElement({ tagName: 'p', className: 'fighter-preview___label' });
  const fighterHealthText = createElement({ tagName: 'p', className: 'fighter-preview___label' });

  fighterName.textContent = fighter.name;
  fighterAttackText.textContent = `Attack: ${fighter.attack}`;
  fighterDefenseText.textContent = `Defense: ${fighter.defense}`;
  fighterHealthText.textContent = `Health: ${fighter.health}`;

  fighterContent.append(
    fighterImage,
    fighterName,
    fighterAttackText,
    fighterAtack,
    fighterDefenseText,
    fighterDefense,
    fighterHealthText,
    fighterHealth
  );
  return fighterContent;
}
