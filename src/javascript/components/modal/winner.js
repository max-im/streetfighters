import App from '../../app';
import { showModal } from './modal';
import { deleteArena } from '../arena';
import { constructfighterContent } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const winnerObj = {
    title: `${fighter.name} WIN!`,
    bodyElement: constructfighterContent(fighter),
    onClose,
  };

  showModal(winnerObj);
}

function onClose() {
  deleteArena();
  new App();
}
